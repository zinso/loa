# HTML parser written in pure Rust,no-std
[![Crates.io](https://img.shields.io/crates/v/loa.svg)](https://crates.io/crates/loa)
[![Rust](https://img.shields.io/badge/rust-1.56.1%2B-blue.svg?maxAge=3600)](https://framagit.org/zinso/loa)
[![license](https://img.shields.io/badge/license-MIT-blue.svg)](https://framagit.org/zinso/loa/-/raw/master/LICENSE)
```
[dependencies]
loa = "0.1.8"
```
## get innner text from node element
 ```rust
fn main(){
    use loa::Getattribute;
    let html = include_str!("../index.html");
    let body = get_first_element_by_tag_name(html, "head").unwrap();
    let text = body.inner_text().unwrap();
    println!("{}",text);
}
 ```
## get node attributes
```rust
fn main() {
    use loa::{get_elements_by_tag_name, get_first_element_by_tag_name, Getattribute};
    let html = include_str!("../index.html");
    let p_list: Vec<String> = loa::get_elements_by_tag_name(html, "p");
    let a_first: String = loa::get_first_element_by_tag_name(html, "a").unwrap();
    let href = a_first.get_attribute("href");
    println!("{:?}", p_list);
    println!("{:?}", a_first);
    println!("{:?}", href);
    let buttons = get_elements_by_tag_name(html, "button");
    for bu in &buttons{
        if bu.contains("Cargo.toml"){
            println!("{:?}",bu.get_attribute("title"));
        }
    }
}    
```
 ## parse html get Vec of nodes
 ```rust
 
 fn main() {
     use loa::{get_elements_by_tag_name, get_first_element_by_tag_name, Getattribute};
     let html = include_str!("../index.html");
     let p_list: Vec<String> = loa::get_elements_by_tag_name(html, "p");
     let a_first: String = loa::get_first_element_by_tag_name(html, "a").unwrap();
     let href = a_first.get_attribute("href");
     let class = a_first.get_attribute("class");
     println!("{:?}", p_list);
     println!("{:?}", a_first);
     println!("{:?}", href);
     println!("{:?}", class);
 }    
 ```
 ## parse html and get first element by tag name
 ```rust
 
 fn main() {
     use loa::{get_elements_by_tag_name, get_first_element_by_tag_name, Getattribute};
     let html = include_str!("../index.html");
     let p_list: Vec<String> = loa::get_elements_by_tag_name(html, "p");
     let a_first: String = loa::get_first_element_by_tag_name(html, "a").unwrap();
     let href = a_first.get_attribute("href");
     let class = a_first.get_attribute("class");
     println!("{:?}", p_list);
     println!("{:?}", a_first);
     println!("{:?}", href);
     println!("{:?}", class);
 }    
 ```
## get all nods by class name
 ```rust

fn main() {
    use loa::{
        get_elements_by_class_name, get_elements_by_tag_name, get_first_element_by_tag_name,
        Getattribute,
    };
    let html = include_str!("../index.html");
    let all_class = get_elements_by_class_name(html, "cake");
    println!("{:#?}",all_class);
}
```
 ## get all a tags href
 ```rust
fn main(){
    use loa::*;
    let html = include_str!("../index.html");
    let hrefs = get_all_a_hrefs(html);
    println!("{:#?}",hrefs.unwrap());
}
 ```
## get first element by class name
 ```rust
fn main() {
    use loa::{
        get_elements_by_class_name, get_elements_by_tag_name, 
        get_first_element_by_tag_name,
        get_first_element_by_class_name,
        Getattribute,
    };
    let html = include_str!("../index.html");
    let class =  get_first_element_by_class_name(html, "cake");
    println!("{:#?}",class);
}
 ```