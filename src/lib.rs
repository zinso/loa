#![no_std]
extern crate alloc;

pub use parse::get_all_a_hrefs;
pub use parse::get_elements_by_class_name;
pub use parse::get_elements_by_tag_name;
pub use parse::get_first_element_by_class_name;
pub use parse::get_first_element_by_tag_name;
pub use traits::Getattribute;
#[allow(warnings)]
mod traits{
    use alloc::string::String;
    use alloc::{fmt::format, string::ToString, vec::Vec};
    use libc_print::std_name::{dbg, eprintln, println};
    pub trait Getattribute {
        fn get_attribute(&self, attr: impl ToString) -> Option<String>;
        fn inner_text(&self) -> Option<String>;
    }

    impl Getattribute for String {
    ///## get innner text from node element
    /// ```rust
    /// #[allow(warnings)]
    ///fn main(){
    ///    use loa::Getattribute;
    ///    let html = include_str!("../index.html");
    ///    let body = get_first_element_by_tag_name(html, "head").unwrap();
    ///    let text = body.inner_text().unwrap();
    ///    println!("{}",text);
    ///}
    /// ```
    fn inner_text(&self) -> Option<String> {
        let pattern = fancy_regex::Regex::new(r#"([^>])[^<>]+(?=[<])"#).unwrap();
        let html = self.to_string().replace("\n", "");
        let mut text_vec = Vec::new();
        for cap in pattern.captures_iter(&html) {
            if let Some(text) = cap
                .unwrap()
                .iter()
                .map(|s| s)
                .collect::<Vec<_>>()
                .first()
                .unwrap()
            {
                text_vec.push(text.as_str().trim().to_string());
            }
        }
        Some(text_vec.join("\n").trim().to_string())
    }
    /// ## get node attributes
    /// ```rust
    /// #[allow(warnings)]
    /// fn main() {
    ///     use loa::{get_elements_by_tag_name, get_first_element_by_tag_name, Getattribute};
    ///     let html = include_str!("../index.html");
    ///     let p_list: Vec<String> = loa::get_elements_by_tag_name(html, "p");
    ///     let a_first: String = loa::get_first_element_by_tag_name(html, "a").unwrap();
    ///     let href = a_first.get_attribute("href");
    ///     println!("{:?}", p_list);
    ///     println!("{:?}", a_first);
    ///     println!("{:?}", href);
    ///     let buttons = get_elements_by_tag_name(html, "button");
    ///     for bu in &buttons{
    ///         if bu.contains("Cargo.toml"){
    ///             println!("{:?}",bu.get_attribute("title"));
    ///         }
    ///     }
    /// }    
    /// ```
    fn get_attribute(&self, attr: impl ToString) -> Option<String> {
        use libc_print::std_name::{dbg, eprintln, println};
        let attr = attr.to_string();
        let out_tag = self
            .split(" ")
            .filter(|s| !s.is_empty())
            .collect::<Vec<_>>()
            .get(0)
            .expect("error to get out tag")
            .to_string()
            .replace("<", "");
        let new_self_vec = self
            .split("><")
            .filter(|s| !s.is_empty())
            .map(|s| s.to_string())
            .collect::<Vec<_>>();
        let new_self = new_self_vec.get(0).unwrap_or(&self).replace("\"\"", "");
        let attr_vec = new_self
            .split("\"")
            .filter(|s| !s.is_empty())
            .map(|s| s.trim().to_string())
            .collect::<Vec<_>>();
        // println!("{:?}", attr_vec);
        let mut attr_index: usize = 0;
        for i in 0..attr_vec.len() {
            let s = attr_vec.get(i).expect("get error");
            if s.contains(&attr) {
                attr_index += 1;
                break;
            }
            attr_index = attr_index + 1;
        }
        match attr_vec.get(attr_index) {
            Some(e) => Some(e.to_string()),
            None => None,
        }
    }
}
}

#[allow(warnings)]
mod parse {
    use alloc::string::String;
    use alloc::vec;
    use alloc::{fmt::format, string::ToString, vec::Vec};
    use libc_print::std_name::{dbg, eprintln, println};

    use crate::Getattribute;

    /// ## parse html get Vec of nodes
    /// ```rust
    /// #[allow(warnings)]
    /// fn main() {
    ///     use loa::{get_elements_by_tag_name, get_first_element_by_tag_name, Getattribute};
    ///     let html = include_str!("../index.html");
    ///     let p_list: Vec<String> = loa::get_elements_by_tag_name(html, "p");
    ///     let a_first: String = loa::get_first_element_by_tag_name(html, "a").unwrap();
    ///     let href = a_first.get_attribute("href");
    ///     let class = a_first.get_attribute("class");
    ///     println!("{:?}", p_list);
    ///     println!("{:?}", a_first);
    ///     println!("{:?}", href);
    ///     println!("{:?}", class);
    /// }    
    /// ```
    pub fn get_elements_by_tag_name(html: impl ToString, tag: impl ToString) -> Vec<String> {
        let html = html.to_string().replace("\n", "");
        let a_b = html
            .split(format(format_args!("</{}>", tag.to_string())).as_str())
            .filter(|s| !s.is_empty())
            .filter(|s| s.contains(format(format_args!("<{}", tag.to_string())).as_str()))
            .map(|s| {
                let a_e = s.replace("\n", "").trim().to_string();
                let a_v = a_e
                    .split(format(format_args!("<{}", tag.to_string())).as_str())
                    .map(|s| s.to_string())
                    .collect::<Vec<_>>();
                let mut a = a_v.get(1).unwrap().to_string();
                a.push_str(format(format_args!("</{}>", tag.to_string())).as_str());
                let mut aa = String::from(format(format_args!("<{} ", tag.to_string())).as_str());
                aa.push_str(&a);
                aa
            })
            .collect::<Vec<_>>();
        a_b
    }
    ///## parse html and get first element by tag name
    /// ```rust
    /// #[allow(warnings)]
    /// fn main() {
    ///     use loa::{get_elements_by_tag_name, get_first_element_by_tag_name, Getattribute};
    ///     let html = include_str!("../index.html");
    ///     let p_list: Vec<String> = loa::get_elements_by_tag_name(html, "p");
    ///     let a_first: String = loa::get_first_element_by_tag_name(html, "a").unwrap();
    ///     let href = a_first.get_attribute("href");
    ///     let class = a_first.get_attribute("class");
    ///     println!("{:?}", p_list);
    ///     println!("{:?}", a_first);
    ///     println!("{:?}", href);
    ///     println!("{:?}", class);
    /// }    
    /// ```
    pub fn get_first_element_by_tag_name(
        html: impl ToString,
        tag: impl ToString,
    ) -> Option<String> {
        if let Some(node) = get_elements_by_tag_name(html, tag).get(0) {
            return Some(node.to_string());
        }
        None
    }
    ///## get all nods by class name
    /// ```ignore
    ///#[allow(warnings)]
    ///fn main() {
    ///    use loa::{
    ///        get_elements_by_class_name, get_elements_by_tag_name, get_first_element_by_tag_name,
    ///        Getattribute,
    ///    };
    ///    let html = include_str!("../index.html");
    ///    let all_class = get_elements_by_class_name(html, "cake");
    ///    println!("{:#?}",all_class);
    ///}
    ///```
    pub fn get_elements_by_class_name(html: impl ToString, class: impl ToString) -> Vec<String> {
        let all_tags = vec![
            "!DOCTYPE",
            "a",
            "abbr",
            "acronym",
            "abbr",
            "address",
            "applet",
            "object",
            "object",
            "area",
            "article",
            "aside",
            "audio",
            "base",
            "basefont",
            "bdi",
            "bdo",
            "big",
            "blockquote",
            "br",
            "button",
            "canvas",
            "caption",
            "center",
            "cite",
            "code",
            "col",
            "colgroup",
            "data",
            "datalist",
            "dd",
            "del",
            "details",
            "dfn",
            "dialog",
            "dir",
            "div",
            "dl",
            "dt",
            "em",
            "embed",
            "fieldset",
            "figcaption",
            "figure",
            "font",
            "frame",
            "frameset",
            "h1",
            "h2",
            "h3",
            "h4",
            "h5",
            "h6",
            "i",
            "iframe",
            "img",
            "input",
            "ins",
            "kbd",
            "label",
            "input",
            "legend",
            "fieldset",
            "li",
            "link",
            "main",
            "map",
            "mark",
            "meta",
            "meter",
            "nav",
            "noframes",
            "noscript",
            "object",
            "ol",
            "optgroup",
            "option",
            "output",
            "p",
            "param",
            "picture",
            "pre",
            "progress",
            "q",
            "rp",
            "rt",
            "ruby",
            "s",
            "samp",
            "script",
            "section",
            "select",
            "small",
            "source",
            "audio",
            "audio",
            "span",
            "strike",
            "del",
            "s",
            "hr",
            "strong",
            "style",
            "sub",
            "summary",
            "details",
            "sup",
            "svg",
            "table",
            "tbody",
            "td",
            "template",
            "textarea",
            "tfoot",
            "th",
            "thead",
            "time",
            "title",
            "tr",
            "track",
            "tt",
            "u",
            "ul",
            "var",
            "video",
            "wbr",
            "footer",
            "form",
            "head",
            "header",
            "html",
            "body",
        ];
        let html = html.to_string().replace("\n", "");
        let mut all_nodes: Vec<_> = vec![];
        for tag in &all_tags {
            let mut nodes = get_elements_by_tag_name(html.to_string(), tag);
            if !nodes.is_empty() {
                all_nodes.append(&mut nodes);
            }
        }
        let mut all_class_nodes = vec![];
        for tag in all_nodes.iter() {
            if tag.contains("class") && tag.contains(&class.to_string()) {
                if tag.get_attribute("class").is_some() {
                    if tag
                        .get_attribute("class")
                        .unwrap()
                        .contains(&class.to_string())
                    {
                        all_class_nodes.push(tag.to_string().trim().to_string());
                    }
                }
            }
        }
        all_class_nodes
    }
    ///## get first element by class name
    /// ```ignore
    /// #[allow(warnings)]
    ///fn main() {
    ///    use loa::{
    ///        get_elements_by_class_name, get_elements_by_tag_name,
    ///        get_first_element_by_tag_name,
    ///        get_first_element_by_class_name,
    ///        Getattribute,
    ///    };
    ///    let html = include_str!("../index.html");
    ///    let class =  get_first_element_by_class_name(html, "cake");
    ///    println!("{:#?}",class);
    ///}
    /// ```
    pub fn get_first_element_by_class_name(
        html: impl ToString,
        class: impl ToString,
    ) -> Option<String> {
        if let Some(node) = get_elements_by_class_name(html, class).get(0) {
            return Some(node.to_string());
        }
        None
    }
    /// ## get all a tags href
    /// ```rust
    ///fn main() {
    ///    let html = include_str!("../index.html");
    ///    let re = loa::get_all_a_hrefs(html);
    ///    println!("{:?}",re.unwrap());
    ///}    
    /// ```
    pub fn get_all_a_hrefs(html: impl ToString) -> Option<Vec<String>> {
        let re_href = fancy_regex::Regex::new(r#"<a .+?\s*href\s*=\s*["']?([^"'\s>]+)["']?"#).unwrap();
        let html = html.to_string();
        let caps = re_href.captures_iter(&html);
        let mut hrefs = vec![];
        for i in caps {
            let cap = i.expect("capture error") ;
            let a = cap.get(0).unwrap().as_str().to_string();
            if a.get_attribute("href").is_some(){
                hrefs.push(a.get_attribute("href").unwrap());
            }
        }
        if hrefs.is_empty() {
            return None;
        } else {
            return Some(hrefs);
        }
    }
}
